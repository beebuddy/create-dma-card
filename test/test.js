import ExportCardUtil from "../src/core/Util/exportCard/ExportCardUtil";

(async () => {
  try {
    await new ExportCardUtil().process(
      "/Users/parintorn/Downloads/tempData.zip",
      "/tmp/result.dmacard"
    )
  } catch (error) {
    console.log("test error :", error);
  }
})();