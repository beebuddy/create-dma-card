import 'react-native';
import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
// test file
import RenderCard from '~/cards/RenderCard';
// fix library
jest.mock('react-native-maps', () => {
  const { View } = require('react-native');
  const MockMapView = (props) => {
    return <View>{props.children}</View>;
  };
  const MockMarker = (props) => {
    return <View>{props.children}</View>;
  };
  return {
    __esModule: true,
    default: MockMapView,
    Marker: MockMarker,
  };
});
// test render
it('renders correctly', () => {
  render(<RenderCard />);
});
