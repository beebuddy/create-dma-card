import EventEmitter from "react-native/Libraries/vendor/emitter/EventEmitter";

class DMAEventHandler {

  constructor() {
    this.eventEmitter = new EventEmitter();
  }

  /**
   * 
   * @param {String} eventName 
   * @param {Function} triggerFunction 
   * @returns {DMAEventObject}
   */
  subscribe(eventName, triggerFunction) {
    return this.eventEmitter.addListener(eventName, triggerFunction);
  }

  /**
   * 
   * @param {String} eventName 
   * @param {Object} params 
   */
  emit(eventName, params) {
    setTimeout(() => {
      this.eventEmitter.emit(eventName, params);
    }, 1);
  }
}

const DMAEvent = new DMAEventHandler();

export default DMAEvent;

class DMAEventObject {
  constructor() {
    this.remove = () => { };
  }
}