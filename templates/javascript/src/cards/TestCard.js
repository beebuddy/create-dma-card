import React from 'react';
import { Platform, SafeAreaView } from 'react-native';
import IDMA from '~/helper/IDMA';
import TestCardBar from './TestCardBar';
import TestCardConfig from './TestCardConfig';

const cardXPathName = TestCardConfig.YOUR_CARD_RULES?.rawCardName || "";
const cardProps = TestCardConfig.YOUR_CARD_RULES?.props || [];
const cardActions = TestCardConfig.YOUR_CARD_RULES?.actions || [];
const cardAPIs = TestCardConfig.YOUR_CARD_RULES?.cardAPI || [];
const cardDynamicStyles = TestCardConfig.YOUR_CARD_RULES?.styled?.dynamicStyle || [];
const testSuites = TestCardConfig.YOUR_CARD_RULES?.testSuite || [];
const ON_WEB_HIDE_CARD_BAR = Platform.OS === 'web' & !(TestCardConfig.EABLE_WEB_CARD_BAR === true ) ;

class TestCard extends React.Component {

  state = {};

  componentDidMount = async () => {
    // setup dynamic actions
    this.dynamicActions = cardActions.reduce((result, item) => {

      result[item.name] = () => { alert(item.name) }

      return result;
    }, {});
    // setup dynamic styles
    this.dynamicStyles = cardDynamicStyles.reduce((result, item) => {

      if(result[item.property_xpath] == null) {
        result[item.property_xpath] = {};
      }

      result[item.property_xpath][item.property_style_name] = item.value || item.defaultValue;

      return result;
    }, {});
    // setup state
    const initialState = {};
    for await (let propData of cardProps) {
      initialState[propData.name] = propData.defaultValue;
    }

    await this.setState({
      [this.props._prefixCardName]: initialState
    });
  }

  onUpdatePropValue = async (props, propName, propValue) => {
    const tempPropData = this.state[props?._prefixCardName];
    if (tempPropData) {
      tempPropData[propName] = propValue;
      this.setState({
        [props?._prefixCardName]: tempPropData
      });
    }
  }

  onRunTestCardAPI = async (testCase) => {
    // close dialog first
    await this.setState({ testCardAPIDialog: false });
    // run test case
    const { sequence } = (testCase || {});
    if (!sequence) {
      alert("Test sequence not found");
      return;
    }

    for await (let testCase of sequence) {
      const result = await IDMA.callEvent(
        this.props._prefixCardName,
        testCase.cardAPIName,
        testCase.input
      );
    }

  }

  render() {
    // setup dynamic props
    const dynamicProps = cardProps.reduce((result, item) => {

      result[item.name] = this.state[this.props._prefixCardName]?.[item.name];

      return result;
    }, {});

    return (
      <SafeAreaView style={{ flex: 1 }}>
        {
          ON_WEB_HIDE_CARD_BAR ? null :
            <TestCardBar
              state={this.state[this.props._prefixCardName]}
              testSuites={testSuites}
              onRunTestCardAPI={this.onRunTestCardAPI}
            />
        }
        <TestCardConfig.YOUR_CARD
          {...this.props}
          {...dynamicProps}
          {...this.dynamicActions}
          xpathName={cardXPathName}
          _dmaDynamicStyle={this.dynamicStyles}
          onUpdatePropValue={this.onUpdatePropValue}
        />
      </SafeAreaView>
    );
  }
}

export default TestCard;