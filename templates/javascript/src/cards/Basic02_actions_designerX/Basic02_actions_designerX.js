import React, { useState } from 'react'
import { View, Text, Image ,TouchableOpacity} from 'react-native'
import { CardCallbackAction } from '~/helper/IDMA';

export default function Basic02_actions_designerX(props) {

    const scale = (props.scale || 1);
    const onClickNext = async (event) => {
        await CardCallbackAction({ props: props, actionName: "onClickNext" })
    }

    return (
        <View
            style={{
                width: props.box_width,
                height: props.box_height,
                backgroundColor: "blue"
            }}
        >
            <Text style={{ fontSize: 16 * scale }}>{props?.fullName}</Text>
            <Image
                style={{
                    width: 50,
                    height: 50
                }}
                source={{
                    uri: props?.myPhoto,
                }}
            />
            <TouchableOpacity
                style={{
                    height: "100%"
                }}
                onPress={onClickNext}
            >
                <Text style={{
                    color: "white",
                    fontWeight: "bold"
                }}>Next
                </Text>
            </TouchableOpacity>

        </View>
    )
}


