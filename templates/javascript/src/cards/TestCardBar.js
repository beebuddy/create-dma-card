import React, { useState } from 'react'
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, FlatList } from 'react-native'

const TestCardBar = ({ state, testSuites = [], onRunTestCardAPI }) => {

  const [checkStateDialog, setCheckStateDialog] = useState(false);
  const [testCardAPIDialog, setTestCardAPIDialog] = useState(false);

  return (
    <React.Fragment>
      <View style={styles.barParent}>
        <TouchableOpacity
          style={styles.barButtonContainer}
          onPress={() => setCheckStateDialog(true)}
        >
          <Text style={styles.barButtonText}>Check State</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.barButtonContainer}
          onPress={() => setTestCardAPIDialog(true)}
        >
          <Text style={styles.barButtonText}>Run Test Case</Text>
        </TouchableOpacity>
      </View>
      <CustomDialog
        title="Current State"
        open={checkStateDialog}
        onClose={() => setCheckStateDialog(false)}
      >
        <Text>{JSON.stringify(state, null, 2)}</Text>
      </CustomDialog>

      <CustomDialog
        title="Test Case"
        open={testCardAPIDialog}
        onClose={() => setTestCardAPIDialog(false)}
      >
        {
          testSuites.length > 0 ? (
            <FlatList
              data={testSuites}
              renderItem={({ item }) => (
                <TouchableOpacity
                  onPress={() => onRunTestCardAPI(item)}
                  style={styles.dialogCardAPIContainer}
                >
                  <Text style={styles.dialogCardAPINameText}>{item.name}</Text>
                </TouchableOpacity>
              )}
              keyExtractor={(item, index) => "testCase_" + index}
              ItemSeparatorComponent={() => (
                <View style={{ height: 20 }} />
              )}
            />
          ) : (
            <Text>No test case.</Text>
          )
        }
      </CustomDialog>
    </React.Fragment>
  )
}

export default TestCardBar

const CustomDialog = (props) => {
  const { open, title, onClose, children } = props;

  if (!open) {
    return null;
  }

  return (
    <View style={styles.dialogBackdrop}>
      <View style={styles.dialogContainer}>
        <Text style={styles.dialogTitleText}>{title}</Text>
        <View style={styles.dialogContent}>
          {children}
        </View>
        <TouchableOpacity
          style={styles.dialogCloseButtonContainer}
          onPress={onClose}
        >
          <Text style={styles.dialogCloseButtonText}>Close</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const window = Dimensions.get("window");

const styles = StyleSheet.create({
  barParent: {
    flexDirection: "row",
    width: "100%",
    height: 40,
    backgroundColor: "black",
    alignItems: "center",
    justifyContent: "space-around"
  },
  barButtonContainer: {
    height: "100%",
    paddingHorizontal: 20,
    justifyContent: "center"
  },
  barButtonText: {
    color: "white",
    fontWeight: "bold"
  },
  dialogBackdrop: {
    position: "absolute",
    width: window.width,
    height: window.height,
    zIndex: 1000,
    backgroundColor: "#00000080",
    alignItems: "center",
    justifyContent: "center"
  },
  dialogContainer: {
    backgroundColor: "white",
    width: "90%",
    borderRadius: 12,
    padding: 20
  },
  dialogContent: {
    marginVertical: 20
  },
  dialogCloseButtonContainer: {
    padding: 10,
    borderRadius: 12,
    height: 50,
    width: "100%",
    backgroundColor: "black",
    alignItems: "center",
    justifyContent: "center",
  },
  dialogCloseButtonText: {
    fontWeight: "bold",
    color: "white"
  },
  dialogTitleText: {
    fontSize: 20,
    fontWeight: "bold"
  },
  dialogCardAPIContainer: {

  },
  dialogCardAPINameText: {
    fontSize: 16,
    color: "blue"
  }
});