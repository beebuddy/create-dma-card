import React, { useState } from 'react'
import { View, Text, Button } from 'react-native'
import { AddCardAPI, CardCallbackAction } from '~/helper/IDMA';

export default function KeyinOTP_designerX(props) {

    const scale = (props.scale || 1);

    const [value, setValue] = useState("");

    /** ============= [Start] Set up Card API  ============= */
    const getValue = async () => {
        return {
            "number": value
        }
    }
    AddCardAPI({
        cardAPIName: "getValue",
        props: props,
        triggerFunction: getValue
    })

    const startCoutdown = async (input) => {
        const timeMs = parseInt(input.timeMs || 0)

        return {}
    }
    AddCardAPI({
        cardAPIName: "startCoutdown",
        props: props,
        triggerFunction: startCoutdown
    })
    /** ============= [END] Set up Card API  ============= */


    const onTimeout = async () => {
        console.log("PRESS onTimeout")
        await CardCallbackAction({ props: props, actionName: "onTimeout" })
        return;
    }

    const onKeyFinish = async () => {
        await CardCallbackAction({ props: props, actionName: "onKeyFinish" })
        return;
    }

    return (
        <View
            style={{
                width: props.box_width,
                height: props.box_height,
                backgroundColor: "#eee"
            }}
        >
            <Text style={{ fontSize: 16 * scale }}>1 2 3 4</Text>
            <Button title="onTimeout" onPress={onTimeout} />
            <Button title="onKeyFinish " onPress={onKeyFinish} />
        </View>
    )
}


