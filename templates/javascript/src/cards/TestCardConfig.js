import YourCard from "./Basic01_props_designerX/Basic01_props_designerX";
import YourCardRules from "./Basic01_props_designerX/DMACARD_Rules.json";

const TestCardConfig = {
  YOUR_CARD: YourCard,
  YOUR_CARD_RULES: YourCardRules?.cardTemplateName ? YourCardRules : null,
  //EABLE_WEB_CARD_BAR : false
};

export default TestCardConfig;