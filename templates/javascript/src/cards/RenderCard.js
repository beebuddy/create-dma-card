import React from 'react';
import IDMA from '~/helper/IDMA';
import { SafeAreaView } from 'react-native';
import TestCardConfig from './TestCardConfig';
import Card from './TestCard';

const deviceScale = IDMA.getDeviceScale();
const prefixCardName = "BX000001_CD000001";
const cardWidth = (TestCardConfig.YOUR_CARD_RULES?.cardInfo?.externalData?.width || 0);
const cardHeight = (TestCardConfig.YOUR_CARD_RULES?.cardInfo?.externalData?.height || 0);

class RenderCard extends React.Component {
  render() {
    return (
      <SafeAreaView>
        <Card
          _prefixCardName={prefixCardName}
          box_height={cardHeight * deviceScale}
          box_width={cardWidth * deviceScale}
        />
      </SafeAreaView>
    );
  }
}

export default RenderCard;