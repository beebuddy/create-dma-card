import React, { useState, useRef } from 'react'
import { View, Text } from 'react-native'
import { AddCardAPI } from '~/helper/IDMA';

export default function Basic03_cardAPI_designerX(props) {

    const scale = (props.scale || 1);

    const [number, setNumber] = useState(0);

    // function
    const handleIncreaseNumberAPI = async (input) => {
        setNumber(number + 1);
        return {};
    }

    const handleDecreaseNumberAPI = async (input) => {
        setNumber(number - 1);
        return {};
    }

    // use ref
    const handleIncreaseNumberAPI_useRef = useRef();
    handleIncreaseNumberAPI_useRef.current = handleIncreaseNumberAPI;

    const handleDecreaseNumberAPI_useRef = useRef();
    handleDecreaseNumberAPI_useRef.current = handleDecreaseNumberAPI;

    // add card api
    AddCardAPI({
        cardAPIName: "increaseNumber",
        props: props,
        triggerFunctionRef: handleIncreaseNumberAPI_useRef
    });

    AddCardAPI({
        cardAPIName: "decreaseNumber",
        props: props,
        triggerFunctionRef: handleDecreaseNumberAPI_useRef
    });

    return (
        <View
            style={{
                width: props.box_width,
                height: props.box_height,
                backgroundColor: "salmon",
                alignItems: "center",
                justifyContent: "center"
            }}
        >
            <Text style={{ fontSize: 30 * scale }}>{number}</Text>
        </View>
    )
}