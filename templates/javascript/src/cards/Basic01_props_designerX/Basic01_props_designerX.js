import React, { useState } from 'react'
import { View, Text, Image } from 'react-native'

export default function Basic01_props_designerX(props) {

    const scale = (props.scale || 1);

    return (
        <View
            style={{
                width: props.box_width,
                height: props.box_height,
                backgroundColor: "grey"
            }}
        >
            <Text style={{ fontSize: 16 * scale }}>{props?.fullName}</Text>
            <Image
                style={{
                    width: 50,
                    height: 50
                }}
                source={{
                    uri:  props?.myPhoto,
                }}
            />

        </View>
    )
}


