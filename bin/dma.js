#!/usr/bin/env node
process.env.ESM_DISABLE_CACHE = true
require = require('esm')(module, { cache: false, cjs: { cache: false }, force: true })
require('../src/cli.js').cli(process.argv);

process.on("uncaughtException", error => {
  console.log(error.message);
  process.exit(0);
});

process.on("unhandledRejection", error => {
  console.log(error);
  process.exit(0);
});