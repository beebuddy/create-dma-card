import { createProject } from './init/InitialProject'
import { exportCard } from './export/ExportProject';
export default class Processor {

    async process(rawArgs) {

        let commandNotFound = `
  Usage: dma [command] [options]

  Options:
  
    -V, --version                     output the version number
    -h, --help                        output usage information
  
  Commands:

    login                             Login to an dma account
    logout                            Logout of an dma account
    register                          Sign up for a new dma account
    whoami                            Return the currently authenticated account

    init [path]                       Create a new dma-card-project
    export                            Export card.dmacard 

  Run a command with --help for more info 💡
    $ dma --help
    
    `

        /**
         * @type {String[]}
         */
        let args = rawArgs.slice(2);
        let listCommand = ["init", "export"];
        let command = args.length > 0 ? args[0] : null;

        if (!command) {
            console.log(commandNotFound)
            return;
        }

        let onlyArg = args.slice(1);

        if (command === "init") {
            createProject(onlyArg[0], onlyArg);
        } else if (command === "export") {
            exportCard(onlyArg[0], onlyArg);
        }
    }

}