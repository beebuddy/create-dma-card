import inquirer from 'inquirer';
import chalk from 'chalk';
import UpdateManager from './core/UpdateManager/UpdateManager';
import { exportCard } from './export/ExportProject';
import { createProject } from './init/InitialProject'
import Processor from './Processor';
//import package from '../package.json'

export async function cli(rawArgs) {
  //let options = parseArgumentsIntoOptions(args);
  //options = await promptForMissingOptions(options);
  // await createProject(options);

  try {
    await UpdateManager.checkUpdate();

    await new Processor().process(rawArgs);
  } catch (error) {
    console.log('Error : %s', chalk.red.bold("" + error));
  }

}

// ...
