import fs from 'fs';
import FormData from "form-data";
import AppConstant from "../../../constant/AppConstant";
import CallAPI from "../network/CallAPI";

class ExportCardUtil {

  /**
   * 
   * @param {String} sourceFilePath 
   * @param {String} outputFilePath 
   */
  async process(sourceFilePath, outputFilePath) {
    // check file exists
    const sourceFileExists = fs.existsSync(sourceFilePath);
    if (!sourceFileExists) {
      throw "export file not found";
    }

    // create request body
    let exportReqBody = new FormData();
    exportReqBody.append("data", fs.createReadStream(sourceFilePath));

    // create request config
    let exportReqConfig = {
      headers: {
        ...exportReqBody.getHeaders(),
        token: AppConstant.API.TOKEN
      }
    };
    // call api export custom card in card-generator-ms
    let exportResp = await CallAPI.post(
      AppConstant.API.URL.EXPORT_CARD,
      exportReqBody,
      exportReqConfig
    );
    // get response data
    let exportRespData = exportResp.data;
    // check response from service
    if (!exportRespData.resultSuccess) {
      throw exportRespData.resultMessage || "Cannot export card";
    }
    // get download url
    let downloadUrl = exportRespData.resultData.downloadUrl;
    // download file to target directory
    await CallAPI.downloadGet(downloadUrl, outputFilePath);
  }
}

export default ExportCardUtil;