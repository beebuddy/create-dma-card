import Axios from "axios";
import fs from "fs";
import logger from "../logger/logger";

const TAG = "[Network]";
class CallAPI {

  /**
   * @param {string} url 
   * @param {JSON} body 
   * @param {AxiosRequestConfig} customConfig
   * @returns {AxiosPromise}
   */
  static async post(url, body, customConfig = null) {
    try {
      /**
     * @type {customConfig}
     */
      let config = this.getNetworkConfig();
      if (customConfig != null) {
        config = { ...config, ...customConfig }
      }
      let response = await Axios.post(url, body, config);
      if (response.status === 200) {
        logger.success(`${TAG} Endpoint url = ${url} , STATUS CODE : ${response.status} , Message : OK`)
      } else {
        logger.error(`${TAG} Endpoint url = ${url} , STATUS CODE : ${response.status} , Message : ${response.statusText}`)
      }
      return response;

    } catch (error) {
      let myError = {
        message: error.message,
        status: error.response ? error.response.status : "",
        data: error.response ? error.response.data : "",
        response: {
          headers: error.response ? error.response.headers : ""
        },
        errorConfig: error.config

      };
      logger.error(`${TAG} Endpoint url = ${url} , STATUS CODE : ${myError.status} , Message : ${"" + myError.message}`)
      throw myError
    }
  }

  /**
   * 
   * @param {String} urlWithParam 
   * @param {AxiosRequestConfig} config 
   */
  static async get(urlWithParam, config = null) {
    try {
      let response = await Axios.get(urlWithParam, config);
      if (response.status === 200) {
        logger.success(`${TAG} Endpoint url = ${urlWithParam} , STATUS CODE : ${response.status} , Message : OK`)
      } else {
        logger.error(`${TAG} Endpoint url = ${urlWithParam} , STATUS CODE : ${response.status} , Message : ${response.statusText}`)
      }
      return response;
    } catch (error) {
      let myError = {
        message: error.message,
        status: error.response ? error.response.status : "",
        data: error.response ? error.response.data : "",
        response: {
          headers: error.response ? error.response.headers : ""
        },
        errorConfig: error.config
      };
      logger.error(`${TAG} Endpoint url = ${urlWithParam} , STATUS CODE : ${myError.status} , Message : ${"" + myError.message}`)
      throw myError;
    }
  }

  /**
     * @returns {AxiosRequestConfig}
     */
  static getNetworkConfig() {
    /**
    * @type {AxiosRequestConfig}
     */
    let config = {
      timeout: 1000 * 60 * 2
    }

    return config

  }

  /**
   * 
   * @param {String} url 
   * @param {String} fileOutput 
   * @param {AxiosRequestConfig} config
   * @returns {Boolean}
   */
  static async downloadGet(url, fileOutput, config = {}) {
    try {
      const mergeConfig = {
        responseType: "stream",
        ...config
      }

      let resultDownload = await this.get(url, mergeConfig);
      let resultDownloadData = resultDownload.data;

      let downloadFileData = fs.createWriteStream(fileOutput);

      await new Promise((resolve, reject) => {
        resultDownloadData.pipe(downloadFileData);
        let error = null;
        downloadFileData.on("error", (error) => {
          error = err;
          downloadFileData.close();
          reject(error);
        });
        downloadFileData.on('close', () => {
          if (!error) {
            resolve(true);
          }
        });
      });

      return true;

    } catch (error) {
      throw error;
    }
  }

}

export default CallAPI;