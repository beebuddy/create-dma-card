

export default {
	log: (...args) => {
		const req = args[0];
		
		// console.log(`\x1b[0m${toString(args).join('')}\x1b[0m`)
	},
	info: (...args) => {
		const req = args[0];
		
		// console.log(`\x1b[36m${toString(args).join('')}\x1b[0m`)
	},
	error: (...args) => {
		const req = args[0];
		
		// console.log(`\x1b[31m${toString(args).join('')}\x1b[0m`)
	},
	warning: (...args) => {
		const req = args[0];
		
		// console.log(`\x1b[33m${toString(args).join('')}\x1b[0m`)
	},
	success: (...args) => {
		const req = args[0];
		
		// console.log(`\x1b[32m${toString(args).join('')}\x1b[0m`)
	},
	magenta: (...args) => {
		const req = args[0];
		
		// console.log(`\x1b[35m${toString(args).join('')}\x1b[0m`)
	},
	random: (...args) => {
		const req = args[0];
		
		// console.log((toString(args).join('').split('').map(item => '\x1b[' + randomColor() + 'm' + item + '\x1b[0m')).join('') + '\n')
	},
	db: (...args) => {
		const req = args[0];
		
		// console.log(`\x1b[35m[Database Message] ${toString(args).join('')}\x1b[0m`)
	},
}
/**
 * Random color not duplicate next character
 */
let tempColor = null;
let randomColor = () => {
	if (tempColor != null) {
		const rand = Math.floor(Math.random() * (37 - 31) + 31);
		if (rand != tempColor) {
			tempColor = rand;
			return rand;
		} else {
			return randomColor();
		}
	} else {
		tempColor = Math.floor(Math.random() * (37 - 31) + 31);
		return tempColor;
	}
}

let toString = (list) => {
	return list.map(item => item.constructor.name == 'Object' || item.constructor.name == 'Array' ? JSON.stringify(item) : item);
}