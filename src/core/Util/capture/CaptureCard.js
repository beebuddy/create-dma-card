// import puppeteer from "puppeteer";

// class CaptureCard {

//   /**
//    * 
//    * @param {String} captureUrl 
//    * @param {String} outputDir 
//    * @param {Number} width 
//    * @param {Number} height 
//    */
//   async process(captureUrl = "http://localhost:19006/", outputDir = "screenshot.png", width = 20, height = 20) {
//     const browser = await puppeteer.launch({
//       defaultViewport: {
//         width: width,
//         height: height,
//         isMobile: true
//       }
//     });

//     try {
//       const page = await browser.newPage();
//       await page.goto(captureUrl);
//       await page.screenshot({ path: outputDir });
//     } catch (error) {
//       throw "Cannot capture card, please make sure web is running"
//     }

//     await browser.close();
//   }
// }

// export default CaptureCard;