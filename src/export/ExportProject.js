import chalk from 'chalk';
import execa from 'execa';
import fs from 'fs';
import gitignore from 'gitignore';
import Listr from 'listr';
import ncp from 'ncp';
import path from 'path';
import { projectInstall } from 'pkg-install';
import license from 'spdx-license-list/licenses/MIT';
import { promisify } from 'util';
import CallAPI from '../core/Util/network/CallAPI';
import AdmZip from 'adm-zip'
//import CaptureCard from '../core/Util/capture/CaptureCard';
import ExportCardUtil from '../core/Util/exportCard/ExportCardUtil';

const access = promisify(fs.access);
const writeFile = promisify(fs.writeFile);
const copy = promisify(ncp);
const writeGitignore = promisify(gitignore.writeFile);


const BUILD_PATH = 'build-card'
const CARD_PATH = 'src/cards'
async function caputerPreviewImage(cardName) {
    
    return;

    let dmaRulesPath = `${CARD_PATH}/${cardName}/DMACARD_Rules.json`
    let read = fs.readFileSync(dmaRulesPath);
    let json = JSON.parse(read);
    let width = json.cardInfo.externalData.width;
    let height = json.cardInfo.externalData.height;
    let screenShotPath = `${BUILD_PATH}/screenShot.png`;

    if (!fs.existsSync(BUILD_PATH)) {
        fs.mkdirSync(BUILD_PATH);

    }

    console.log("cardTemplateName = ", json.cardTemplateName);
    console.log("props = ", json.props.length);
    console.log("actions = ", json.actions.length);
    console.log("cardAPI = ", json.cardAPI.length);
    //await new CaptureCard().process("http://localhost:19006/", screenShotPath, width, height);



    if(!fs.existsSync(screenShotPath)){
        throw `${screenShotPath} is not found`
    }

    return;
}

async function sendToCardGenerator(cardName) {
    let buildPath = BUILD_PATH;
    let zipFilePath = `${buildPath}/tempData.zip`
    let cardPath = `${CARD_PATH}/${cardName}`
    let screenShotPath = `${BUILD_PATH}/screenShot.png`;
    let resultExportFilePath = `${buildPath}/${cardName}.dmacard`;
    let originalScreenShotPath = `${cardPath}/screenShot.png`;

    if (!fs.existsSync(buildPath)) {
        fs.mkdirSync(buildPath);
    }

    if(!fs.existsSync(originalScreenShotPath)){
        throw `screenShot.png is not found`
    }



    var zip = new AdmZip();
    //zip.addLocalFile(screenShotPath);
    zip.addLocalFolder(cardPath);
    zip.writeZip(zipFilePath);

    await new ExportCardUtil().process(zipFilePath, resultExportFilePath);

    return;
}

/**
 * 
 * @param {String} cardName 
 * @param {String[]} args 
 * @returns 
 */
export async function exportCard(cardName, args) {



    let options = {
        email: 'demo@example.dev',
        name: 'Demo demo',

    };


    const tasks = new Listr(
        [
            {
                title: 'create preview image',
                task: () => caputerPreviewImage(cardName),
            },
            {
                title: 'send to card generator',
                task: () => sendToCardGenerator(cardName),
            }
        ],
        {
            exitOnError: true,
        }
    );

    await tasks.run();
    console.log('%s Export ', chalk.green.bold('DONE'));
    console.log(`Export file is in build-card directory`);
    return true;
}
